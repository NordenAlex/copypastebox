package ru.sendler.copypastebox.api.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import ru.sendler.copypastebox.api.request.PublicStatus;
@Data
@RequiredArgsConstructor
public class PasteboxResponse {
    private final String data;
    private final boolean isPublic;
}
