package ru.sendler.copypastebox.api.request;

public enum PublicStatus {
    PUBLIC,
    UNLISTED
}
