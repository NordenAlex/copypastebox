package ru.sendler.copypastebox.service;

import ru.sendler.copypastebox.api.request.PasteBoxRequest;
import ru.sendler.copypastebox.api.response.PasteboxResponse;
import ru.sendler.copypastebox.api.response.PasteboxUrlResponse;

import java.util.List;

public interface PasteboxService {
    PasteboxResponse getByHash(String hash);
    List<PasteboxResponse> getFirstPublicPasteboxes();
    PasteboxUrlResponse create(PasteBoxRequest request);
}
