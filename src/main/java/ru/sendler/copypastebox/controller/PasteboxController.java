package ru.sendler.copypastebox.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.sendler.copypastebox.api.request.PasteBoxRequest;
import ru.sendler.copypastebox.api.response.PasteboxResponse;
import ru.sendler.copypastebox.api.response.PasteboxUrlResponse;
import ru.sendler.copypastebox.service.PasteboxService;

import java.util.Collection;
import java.util.Collections;

@RestController
@RequiredArgsConstructor
public class PasteboxController {
    private final PasteboxService pasteboxService;


    @GetMapping("/")
    public Collection<PasteboxResponse> getPublicPasteList(){
        return pasteboxService.getFirstPublicPasteboxes();
    }

    @GetMapping("/{hash}")
    public PasteboxResponse getByHash(@PathVariable String hash){
        return pasteboxService.getByHash(hash);
    }

    @PostMapping("/")
    public PasteboxUrlResponse add(@RequestBody PasteBoxRequest request){
        return pasteboxService.create(request);
    }
}
