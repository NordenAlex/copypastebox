package ru.sendler.copypastebox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CopypasteboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(CopypasteboxApplication.class, args);
	}

}
