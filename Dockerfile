FROM openjdk:14-jdk-alpine
MAINTAINER Alexey Nikolaev
COPY target/copypastebox-0.0.1-SNAPSHOT.jar copypastebox.jar
ENRYPOINT ["java","-jar", "/copypastebox.jar"]